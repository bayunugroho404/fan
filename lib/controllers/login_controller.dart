import 'package:fan/screens/view/home_page.dart';
import 'package:fan/utils/util_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class LoginController extends GetxController {
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController cPasswordController = new TextEditingController();

  void doLogin(BuildContext context, String email, String password) {
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    if (email.isNotEmpty && password.isNotEmpty) {
      if (emailValid) {
        Get.offAndToNamed("/home");
      } else {
        showToast(context, "Periksa Email Kembali");
      }
    } else {
      showToast(context, "isi data terlebih dahulu");
    }
  }
}
