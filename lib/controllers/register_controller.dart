import 'package:fan/screens/view/home_page.dart';
import 'package:fan/utils/util_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class RegisterController extends GetxController {
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController cPasswordController = new TextEditingController();

  void doRegister(BuildContext context, String name, String email, String password,
      String cPassword) {
    bool emailValid = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    if (name.length < 3) {
      showToast(context, "minimal tiga karakter");
    } else if (name.length > 50) {
      showToast(context, "maximal 50 karakter");
    } else if (password != cPassword) {
      showToast(context, "Password harus sama ");
    } else {
      if (emailValid) {
        Get.toNamed("/login");
      } else {
        showToast(context, "Pastikan email benar");
      }
    }
  }


}
