import 'package:fan/screens/view/home_page.dart';
import 'package:fan/screens/view/login_page.dart';
import 'package:fan/screens/view/register_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: RegisterPage(),
        getPages: [
          GetPage(
            name: '/login',
            page: () => LoginPage(),
            transition: Transition.zoom,
          ),
          GetPage(
            name: '/home',
            page: () => HomePage(),
            transition: Transition.zoom,
          ),

        ]);
  }
}
