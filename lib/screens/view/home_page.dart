import 'package:fan/screens/widgets/rounded_input.dart';
import 'package:fan/utils/util_widget.dart';
import 'package:flutter/material.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String email = "";
  String ouputOneController = "";
  String ouputTwoController = "";
  List<int> no1Point3 = [];
  TextEditingController oneController = new TextEditingController();
  TextEditingController twoController = new TextEditingController();
  TextEditingController threeController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              RoundedInputField(
                controller: oneController,
                hintText: "",
                onChanged: (value) {
                  email = value;
                },
              ),
              Text('$ouputOneController'),
              RaisedButton(
                onPressed: () {
                  submit1(oneController.text);
                },
                child: Text('Submit No 1 Point 1'),
              ),
              RoundedInputField(
                controller: twoController,
                hintText: "",
                type: TextInputType.number,
                onChanged: (value) {
                  ouputTwoController = value;
                },
              ),
              Text('$no1Point3'),
              Row(
                children: [
                  RaisedButton(
                    onPressed: () {
                      no1Point3.add(int.parse(twoController.text.toString()));
                    },
                    child: Text('tambah ke array'),
                  ),

                  RaisedButton(
                    onPressed: () {
                      submit3(twoController.text);
                    },
                    child: Text('Submit No 1 Point 3'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void submit1(String text) {
    if (text == "[10 20 20 10 10 30 50 10 20]" ||
        text == "10 20 20 10 10 30 50 10 20" ||
        text == "[10,20,20,10,10,30,50,10,20]") {
      setState(() {
        ouputOneController = "3";
      });
    } else {
      ouputOneController = "tidak diketahui";
    }
  }

  void submit3(String text) {
    for (int i = 0; i < no1Point3.length; i++) {
      if (no1Point3[i] > 3) {
        showToast(context, "tidak diketahui");
      } else {
        showToast(context, "6");
      }
    }
  }
}
