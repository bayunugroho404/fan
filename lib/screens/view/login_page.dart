import 'package:fan/controllers/login_controller.dart';
import 'package:fan/screens/widgets/rounded_button.dart';
import 'package:fan/screens/widgets/rounded_input.dart';
import 'package:fan/screens/widgets/rounded_password.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({
    Key key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email ="";
  String password ="";
  final LoginController _loginController = Get.put(LoginController());



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.height * 0.03),
                Lottie.asset(
                  "assets/img/login.json",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  controller: _loginController.emailController,
                  hintText: "Email",
                  type: TextInputType.text,
                  onChanged: (value) {
                    email = value;
                  },
                ),
                RoundedPasswordField(
                  controller:_loginController.passwordController,
                  onChanged: (value) {
                    password = value;
                  },
                ),
                RoundedButton(
                  text: "LOGIN",
                  press: () {
                    _loginController.doLogin(context, email, password);
                  },
                ),
                SizedBox(height: size.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
