import 'package:fan/controllers/login_controller.dart';
import 'package:fan/controllers/register_controller.dart';
import 'package:fan/screens/widgets/rounded_button.dart';
import 'package:fan/screens/widgets/rounded_input.dart';
import 'package:fan/screens/widgets/rounded_password.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({
    Key key,
  }) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String email ="";
  String password ="";
  String cPassword ="";
  String name ="";
  final RegisterController _controller = Get.put(RegisterController());



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.height * 0.03),
                Lottie.asset(
                  "assets/img/login.json",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  controller: _controller.nameController,
                  hintText: "name",
                  type: TextInputType.text,
                  onChanged: (value) {
                    name = value;
                  },
                ),
                RoundedInputField(
                  controller: _controller.emailController,
                  type: TextInputType.text,
                  hintText: "Email",
                  onChanged: (value) {
                    email = value;
                  },
                ),
                RoundedPasswordField(
                  hintText: "Passowrd",

                  controller:_controller.passwordController,
                  onChanged: (value) {
                    password = value;
                  },
                ),
                RoundedPasswordField(
                  hintText: "Confirm Passowrd",
                  controller:_controller.cPasswordController,
                  onChanged: (value) {
                    cPassword = value;
                  },
                ),
                RoundedButton(
                  text: "LOGIN",
                  press: () {
                    _controller.doRegister(context, name, email, password, cPassword);
                  },
                ),
                SizedBox(height: size.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
